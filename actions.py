from helper import write_to_file
from manager import Manager


manager = Manager()


@manager.assign("saldo")
def saldo(change: int, description: str, output_file):
    change_in_account = int(change)
    manager.balance += change_in_account
    manager.logs.append(["saldo", change_in_account, description])
    write_to_file(output_file, manager.logs[-1])


@manager.assign("zakup")
def zakup(prod_id, util_price, purch_nr_items, output_file):
    purchase_product_ID = prod_id
    purchase_until_price = int(util_price)
    purchase_nr_items = int(purch_nr_items)

    if purchase_until_price * purchase_nr_items > manager.balance:
        print("Insufficient funds on the account.")
        return

    if purchase_product_ID in manager.warehouse:

        manager.warehouse[purchase_product_ID] += purchase_nr_items
    else:
        manager.warehouse[purchase_product_ID] = purchase_nr_items
    manager.balance -= purchase_until_price * purchase_nr_items
    manager.logs.append(["zakup", manager.warehouse, manager.balance])
    write_to_file(output_file, manager.logs[-1])


@manager.assign("sprzedaz")
def sprzedaz(prod_id, unit_price, nr_items, output_file):
    sale_product_ID = prod_id
    sale_unit_price = int(unit_price)
    sale_nr_items = int(nr_items)

    if sale_unit_price < 0:
        print("Blad. Cena nie moze byc ujemna")
        return

    if sale_product_ID in manager.warehouse:
        manager.warehouse[sale_product_ID] -= sale_nr_items
    else:
        print('Brak towaru w magazynie')
    manager.balance += sale_unit_price * sale_nr_items
    manager.logs.append(["sprzedaz", manager.warehouse, manager.balance])
    write_to_file(output_file, manager.logs[-1])


def perform_actions_from_file(lines, output_file):
    for i, line in enumerate(lines):
        if line == "saldo":
            manager.actions["saldo"](int(lines[i + 1]), lines[i + 2], output_file)

        elif line == "zakup":
            manager.actions["zakup"](lines[i + 1], lines[i + 2], lines[i+3], output_file)

        elif line == "sprzedaz":
            manager.actions["sprzedaz"](lines[i + 1], lines[i + 2], lines[i+3], output_file)

