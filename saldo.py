from helper import read_from_file, drop_file_data
import sys
import actions

args_from_call = sys.argv[1:]
file_name = args_from_call[0]
value = args_from_call[1]
description = args_from_call[2]

drop_file_data('output.txt')
lines_from_file = read_from_file('input.txt')
actions.perform_actions_from_file(lines_from_file, output_file=file_name)


print("Wybrano tryb saldo.")
actions.manager.actions["saldo"](value, description, file_name)
