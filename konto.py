from helper import write_to_file, read_from_file
import sys
import actions

args_from_call = sys.argv[1:]
file_name = args_from_call[0]
lines_from_file = read_from_file('input.txt')
actions.perform_actions_from_file(lines_from_file, file_name)


print("Wybrales tryb konto.")
print(f"Stan konta wynosi: {actions.manager.balance}")
actions.manager.logs.append(['konto', actions.manager.balance, "Sprawdzenie stanu konto"])
write_to_file(file_name, str(actions.manager.logs[-1]))
