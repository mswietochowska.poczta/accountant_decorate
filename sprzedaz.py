from helper import read_from_file, drop_file_data
import sys
import actions

args_from_call = sys.argv[1:]
output_file = args_from_call[0]

drop_file_data(output_file)

lines_from_file = read_from_file("input.txt")
actions.perform_actions_from_file(lines_from_file, output_file)

print("Wybrano tryb sprzedaży.")
sale_product_ID = args_from_call[1]
sale_unit_price = int(args_from_call[2])
sale_nr_items = int(args_from_call[3])

actions.manager.actions["sprzedaz"](sale_product_ID, sale_unit_price, sale_nr_items, output_file)
