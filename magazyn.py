from helper import write_to_file, read_from_file
import sys
import actions

args_from_call = sys.argv[1:]

file_name = args_from_call[0]
id_products = args_from_call[1:]

lines_from_file = read_from_file('input.txt')
actions.perform_actions_from_file(lines_from_file, output_file=file_name)


print("Wybrales tryb magazyn")

for product in id_products:
    id_product = product
    description = f"Sprawdzenie stanu magazynowego dla {id_product}"
    if id_product not in actions.manager.warehouse:
        stan = 'brak'
    else:
        stan = actions.manager.warehouse[id_product]
    actions.manager.logs.append(['magazyn', id_product, stan, description])
    write_to_file(file_name, str(actions.manager.logs[-1]))
