class Manager:
    def __init__(self):
        self.actions = {}
        self.balance = 1000
        self.warehouse = {}
        self.logs = []

    def assign(self, part):
        def decorate(callback):
            self.actions[part] = callback

        return decorate
