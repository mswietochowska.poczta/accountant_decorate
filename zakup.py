from helper import read_from_file, drop_file_data
import sys
import actions

args_from_call = sys.argv[1:]
output_file = args_from_call[0]

drop_file_data(output_file)
lines_from_file = read_from_file('input.txt')
actions.perform_actions_from_file(lines_from_file, output_file)

print("Wybrano tryb zakupu.")
purchase_product_ID = args_from_call[1]
purchase_until_price = int(args_from_call[2])
purchase_nr = int(args_from_call[3])

actions.manager.actions["zakup"](purchase_product_ID, purchase_until_price, purchase_nr, output_file)
