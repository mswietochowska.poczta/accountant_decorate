import os


def read_from_file(file_name):
    if not os.path.isfile(file_name):
        print("Podana nazwa pliku nie istnieje")

    with open(file_name, 'r') as file:
        result = []
        for line in file:
            result.append(line.rstrip())
        return result


def write_to_file(file_name, data):
    with open(file_name, 'a') as file:
        file.write(f"{data}\n")


def drop_file_data(file_name):
    if not os.path.isfile(file_name):
        return

    with open(file_name, 'w') as f:
        f.truncate()
